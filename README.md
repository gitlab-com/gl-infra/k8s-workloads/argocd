# Argocd PoC

This repo is for [Argo CD](https://argo-cd.readthedocs.io/en/stable/) PoC as part of the larger [Gitops PoC](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/829).   

## Login

- [Argocd UI](https://argocd.pre.gitlab.net/).  

Login is supported via Google SSO, but is limited to read only at this time.  
There is some PoC admin credentials in 1password which should autofill as well.  

## Repository Structure

For the sake of this PoC, this repo is a mono repo of helm charts.  
With the core chart being .`./argocd`, which templates and installs the argocd instance in use.  
Additional charts can be added at the top level, for example `./external-dns`.  

Applications can also be added pointing to any git repo source, however you will need to ensure the [appropriate credentials exist](#repository-credentials).  

## Adding Applications

Applications can be added by adding a new Application manifest to `./argocd/templates/apps/`, or `./argocd/templates/appsets/` for ApplicationSets.  

- [Application Docs](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/#applications).  
- [Application Manifest Example](https://argo-cd.readthedocs.io/en/stable/operator-manual/application.yaml).  
- [ApplicationSet Docs](https://argo-cd.readthedocs.io/en/stable/operator-manual/applicationset/).  

Application sets use the same manifest as Applicatiosn but it is templated within the ApplicationSet. See Docs for more info.  

## Application Deployment

To replicate some existing workflows for the PoC, we have enabled Gitlab CI/CD to auto sync the apps on change.  
If you wish to disable this feature to test Argocd auto-sync/handling of the CD component, you can add an application label `gitlab.io/auto-sync: "false"`.  

## Repository Credentials

If adding an application that requires specific credentials to git/helm/oci registry, you will also need to add the repo credentials.  
Additional credentials can be added via `./argocd/templates/repos/`.   

- [Repository Credential Docs](https://argo-cd.readthedocs.io/en/latest/operator-manual/declarative-setup/#repository-credentials).  
